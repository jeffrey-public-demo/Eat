﻿using DataAccess.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace DataAccess
{
    public class MyContext : DbContext
    {
        public MyContext(DbContextOptions<MyContext> options) : base(options)
        {
            
        }

        public DbSet<AreaModel> Areas { get; set; }

        public DbSet<FoodModel> Foods { get; set; }

    }
}
