﻿using System;

namespace DataAccess.Models
{
    /// <summary>
    /// 地区
    /// </summary>
    public class AreaModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public bool IsEnable { get; set; }

    }
}
