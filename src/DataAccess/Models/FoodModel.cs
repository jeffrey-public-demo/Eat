﻿using System;

namespace DataAccess.Models
{
    /// <summary>
    /// 食物
    /// </summary>
    public class FoodModel
    {
        public Guid Id { get; set; }

        public string Name { get; set; }

        public DiningType DiningType { get; set; }

        public bool IsEnable { get; set; }

        public DateTime DateTime { get; set; }

    }

    /// <summary>
    /// 就餐类别
    /// </summary>
    public enum DiningType
    {
        Beforedawn,
        Breakfast,
        Lunch,
        AfternoonTea,
        Dinner,
        NightSnack,
        Unknown
    }
}
