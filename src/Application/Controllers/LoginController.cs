﻿using Application.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Application.Controllers
{
    public class LoginController : Controller
    {

        public string ErrorMessage { get; set; }

        public IActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Validate(IFormCollection formCollection)
        {
            string username = formCollection["username"];
            string password = formCollection["password"];
            bool remember = string.IsNullOrEmpty(formCollection["remember"]);

            return RedirectToAction("Index");
        }
    }
}
