﻿using Application.Helpers;
using Application.Models;
using DataAccess;
using DataAccess.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Distributed;
using Microsoft.Extensions.Caching.StackExchangeRedis;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Cryptography;
using System.Threading.Tasks;

namespace Application.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly MyContext _myCotext;
        private readonly IDistributedCache _cache;
        private IHttpContextAccessor _accessor;

        private IDictionary<string, List<FoodModel>> userFoods;

        public string Message { get; set; }

        public HomeController(ILogger<HomeController> logger, MyContext myCotext, IDistributedCache cache, IHttpContextAccessor accessor)
        {
            _logger = logger;
            _myCotext = myCotext;
            _cache = cache;
            _accessor = accessor;
        }

        [HttpPost]
        public object AddFood(string foodName)
        {
            try
            {
                string ipAddress = _accessor.HttpContext.Connection.RemoteIpAddress.ToString();
                string ipCache = _cache.GetString($"IpRateLimiting:{{{ipAddress}}}");
                IpRateLimiting ipRate;
                if (string.IsNullOrWhiteSpace(ipCache))
                {
                    ipRate = new IpRateLimiting()
                    {
                        RemoteIpAddress = ipAddress,
                        VisitsCount = 1,
                        AddFoodCount = 0,
                        LastDateTime = DateTime.Now,
                    };
                    _cache.SetString($"IpRateLimiting:{{{ipAddress}}}", JsonConvert.SerializeObject(ipRate));
                }
                else
                {
                    ipRate = JsonConvert.DeserializeObject<IpRateLimiting>(ipCache);
                    ipRate.AddFoodCount = ipRate.AddFoodCount + 1;
                    ipRate.LastDateTime = DateTime.Now;
                    _cache.SetString($"IpRateLimiting:{{{ipAddress}}}", JsonConvert.SerializeObject(ipRate));
                }

                if (ipRate.AddFoodCount >= 30 && !IsAuthorization())
                {
                    return new { Code = 403, Message = "未授权用户，无法继续添加Food" };
                }
                if (string.IsNullOrEmpty(foodName))
                {
                    return new { Code = 500, Message = "找不到 FoodName" };
                }
                if (foodName.Length > 20)
                {
                    return new { Code = 500, Message = "Food 名字太长了" };
                }

                foodName = foodName.Trim();

                AddCacheFood(ipAddress, foodName);
            }
            catch (Exception ex)
            {
                return new { Code = 200, Message = ex.Message };
            }
            return new { Code = 200 };
        }

        private void AddCacheFood(string ipAddress, string foodName)
        {
            var cacheStr = _cache.GetString($"Foods:{{{ipAddress}}}");
            List<FoodModel> cacheFoods = new List<FoodModel>();
            if (!string.IsNullOrEmpty(cacheStr))
            {
                cacheFoods.AddRange(JsonConvert.DeserializeObject<List<FoodModel>>(cacheStr));
            }

            var food = new FoodModel()
            {
                Id = Guid.NewGuid(),
                IsEnable = true,
                Name = foodName,
                DiningType = GetDiningType(),
                DateTime = DateTime.Now
            };

            if (cacheFoods.FirstOrDefault(x => x.Name == foodName) != null)
            {
                return;
            }

            cacheFoods.Add(food);

            if (IsAuthorization())
            {
                _myCotext.Foods.Add(food);
                _myCotext.SaveChanges();
            }

            _cache.SetString($"Foods:{{{ipAddress}}}", JsonConvert.SerializeObject(cacheFoods));
        }

        public bool IsAuthorization()
        {
            var username = Request.Cookies["dm_username"];
            if (username != null && MD5(username) == "6aa8d4d501a7e1d67c6dbf8c8138b897")
            {
                return true;
            }
            return false;
        }

        public string MD5(string str)
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            byte[] bytValue, bytHash;
            bytValue = System.Text.Encoding.UTF8.GetBytes(str);
            bytHash = md5.ComputeHash(bytValue);
            md5.Clear();
            string sTemp = "";
            for (int i = 0; i < bytHash.Length; i++)
            {
                sTemp += bytHash[i].ToString("X").PadLeft(2, '0');
            }
            str = sTemp.ToLower();

            return str;
        }

        public DiningType GetDiningType()
        {
            var tiem = DateTime.Now;

            if (tiem.Hour >= 3 && tiem.Hour < 6)
            {
                return DiningType.Beforedawn;
            }
            else if (tiem.Hour >= 6 && tiem.Hour <= 10)
            {
                return DiningType.Breakfast;
            }
            else if (tiem.Hour >= 10 && tiem.Hour <= 14)
            {
                return DiningType.Lunch;
            }
            else if (tiem.Hour >= 14 && tiem.Hour < 17)
            {
                return DiningType.AfternoonTea;
            }
            else if (tiem.Hour >= 17 && tiem.Hour <= 21)
            {
                return DiningType.Dinner;
            }
            else if (tiem.Hour >= 21 || tiem.Hour <= 3)
            {
                return DiningType.NightSnack;
            }

            return DiningType.Unknown;
        }

        public List<FoodModel> GetDefaultFoods()
        {
            List<FoodModel> foods = new List<FoodModel>();

            foods.AddRange(_myCotext.Foods.Where(w => w.DiningType == GetDiningType()).ToList());

            return foods;
        }

        public IActionResult Index()
        {
            DiningType type = GetDiningType();

            List<FoodModel> Foods = GetDefaultFoods();

            var userId = Request.Cookies["dm_userid"];
            if (userId != null)
            {
                var cacheStr = _cache.GetString($"Foods:{{{userId}}}");
                if (!string.IsNullOrEmpty(cacheStr))
                {
                    Foods.AddRange(JsonConvert.DeserializeObject<List<FoodModel>>(cacheStr));
                }
            }

            if (Foods.Count > 0)
            {
                // 显示的消息
                var foodName = Foods[new Random().Next(0, Foods.Count)].Name;
                switch (type)
                {
                    case DiningType.Beforedawn:
                        Message = $"这么早起来，要吃丰盛一点哦！吃 {foodName} 怎么样！";
                        break;
                    case DiningType.Breakfast:
                        Message = $"早上好, 吃 {foodName} 怎么样！";
                        break;
                    case DiningType.Lunch:
                        Message = $"中午, 吃 {foodName} 怎么样！";
                        break;
                    case DiningType.AfternoonTea:
                        Message = $"来点下午茶吧， {foodName} 怎么样！ 就酱？";
                        break;
                    case DiningType.Dinner:
                        Message = $"晚上，吃 {foodName} 怎么样！ 就酱？";
                        break;
                    case DiningType.NightSnack:
                        Message = $"想啥呢？还想吃宵夜，还不够胖的吗 ！";
                        break;
                    case DiningType.Unknown:
                        break;
                    default:
                        break;
                }
            }

            ViewBag.Message = Message;

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Complaints()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }

    /// <summary>
    /// IP速率限制
    /// </summary>
    public class IpRateLimiting
    {
        public string RemoteIpAddress { get; set; }

        public int VisitsCount { get; set; }

        public int AddFoodCount { get; set; }

        public DateTime LastDateTime { get; set; }
    }
}
